#Gulosity Web Platform

Java快速开发平台

Feature:

1. Session管理器ISessionManager，默认实现`com.opdar.platform.core.session.MemorySessionManager`
2. 控制器与路由层拦截器`com.opdar.platform.core.base.Interceptor`，调用顺序为先进后出
3. 错误管理器`com.opdar.platform.core.base.ErrorHandler`，默认不打印路由层抛出错误，需要拦截后自行打印。
4. 默认集成Jetty 9.2.10
5. Restful风格路由支持`{{variable}}`
6. 支持JSON、XML、BINARY输出
7. 默认集成`commons-fileupload`
8. 可替换的渲染引擎`com.opdar.platform.core.base.ViewTemplate`，默认集成freemarker渲染，支持jsp渲染

Maven:
```
<dependency>
    <groupId>com.opdar.platform</groupId>
    <artifactId>gulosity-platform</artifactId>
    <version>1.1.4</version>
</dependency>
```

打算做基于openresty网关和java+nw的一体化中台，与框架形成高效管理的功能，一个人实在来不及维护。

目前已经有但尚不成熟的中台还未开源，主要功能大概有，项目管理，网关API管理，服务模块管理，接口管理，类Postman的工具，服务调用链追溯，本地Mock服务（客户端模式适用），环境变量管理，以及一系列认证api，方便联通内网及后台的认证工作，支持LDAP，但与现有技术环境结合比较紧密，不适合拿出来，有想法的可以一起纯技术交流一下，私可。