package com.opdar.platform.annotations;

import com.opdar.platform.core.base.SecretType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by shiju on 2017/1/17.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Request {

    public enum Method{
        GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE ,ALL
    }
    public enum Format{
        XML,JSON,STREAM,VIEW
    }
    String secretKey() default "123456";
    String value() default "";
    boolean restful() default false;
    int version() default 0;
    Method method() default Method.ALL;
    SecretType secret() default SecretType.NONE;
    Format format() default Format.JSON;
    boolean hasToken() default false;
}
