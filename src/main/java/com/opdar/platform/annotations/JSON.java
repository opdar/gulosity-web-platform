package com.opdar.platform.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by shiju on 2017/1/23.
 * 如需自定义content-type，请访问 com.opdar.platform.annotations.SerializeBody
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface JSON {
}
