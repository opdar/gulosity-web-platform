package com.opdar.platform.gateway.base;

import com.opdar.platform.gateway.protocol.IProtocol;

public class ApiServiceManager {
    private static <T extends IProtocol> IProtocol createProtocol(Class<T> protocolClz){
        T protocol = null;
        try {
            protocol =  protocolClz.newInstance();
        } catch (Exception ignored) {}
        return protocol;
    }

    public static <T extends IProtocol> IProtocol create(Class<T> protocolClz, String host, Integer port , String module){
        IProtocol protocol = ApiServiceManager.createProtocol(protocolClz);
        if(protocol != null){
            return protocol.create(host,port,"/api/"+module+"/");
        }
        return null;
    }

}
