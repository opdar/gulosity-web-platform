package com.opdar.platform.gateway.protocol;

import com.opdar.platform.core.base.Router;
import com.opdar.platform.core.base.RouterManager;
import com.opdar.platform.gateway.base.NameValuePair;
import com.opdar.platform.gateway.point.PointManager;
import org.springframework.util.StringUtils;

import java.util.*;

import static com.opdar.platform.gateway.protocol.ProtocolHeader.ApiHeaderKey.*;


public class ProtocolHeader {
    private StringBuilder logBuilder = new StringBuilder();
    private List<NameValuePair> valuePairs = new LinkedList<NameValuePair>();
    public static final class ApiHeaderKey{
        public static final String ROUTER_NAME = "X-Router";
        public static final String LINK_UNIQUE_ID = "X-Api-UniqueID";
        public static final String LINK_DEEP = "X-Api-Deep";
        public static final String ROUTER_CODE = "X-Gateway-Code";
    }

    private ProtocolHeader(String uniqueId,String deep) {
            Router router1 = RouterManager.getCurrentRouter();
            if(router1!= null){
                String routerName = router1.getName();
                //追加当前路由名称
                append(ROUTER_NAME,routerName);
            }
            try{
                if(StringUtils.isEmpty(uniqueId)){
                    uniqueId = UUID.randomUUID().toString();
                }
                //追加唯一链路ID
                append(LINK_UNIQUE_ID,uniqueId);
                int _deep = 0;
                if(!StringUtils.isEmpty(deep)){
                    _deep = Integer.valueOf(deep)+1;
                }
                //追加链路深度
                append(LINK_DEEP,String.valueOf(_deep+1));
            }catch (Exception ignored){}
            String code = PointManager.getCurrentCode();
            append(ROUTER_CODE,code);
    }

    public static ProtocolHeader build(String uniqueId,String deep) {
        return new ProtocolHeader(uniqueId,deep);
    }

    public List<NameValuePair> getValuePairs() {
        return valuePairs;
    }

    private void append(String key, String value){
        valuePairs.add(new NameValuePair(key,value));
        logBuilder.append(key).append("=").append(value).append("|");
    }

    @Override
    public String toString() {
        return logBuilder.toString();
    }
}
