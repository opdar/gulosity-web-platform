package com.opdar.platform.gateway.protocol;

import com.opdar.platform.gateway.base.NameValuePair;

public interface IProtocol {
    IProtocol create(String host, Integer port, String apiModule);
    byte[] invoke(String router, NameValuePair...params);
}
