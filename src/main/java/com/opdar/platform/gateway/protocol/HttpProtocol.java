package com.opdar.platform.gateway.protocol;

import com.opdar.platform.core.base.Context;
import com.opdar.platform.gateway.base.NameValuePair;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.opdar.platform.gateway.protocol.ProtocolHeader.ApiHeaderKey.*;

public class HttpProtocol implements IProtocol {
    private Logger logger = LoggerFactory.getLogger(HttpProtocol.class);

    private String url;

    private boolean ssl;

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    @Override
    public IProtocol create(String host, Integer port, String apiModule) {
        url = String.format("%s%s:%d%s",ssl?"https://":"http://",host,port,apiModule);
        return this;
    }

    @Override
    public byte[] invoke(String router, NameValuePair...params) {
        OkHttpClient client = new OkHttpClient();
        Request.Builder build = new Request.Builder().url(url+router);
        String uniqueId = Context.getRequest().getHeader(LINK_UNIQUE_ID);
        String deep = Context.getRequest().getHeader(LINK_DEEP);
        ProtocolHeader protocolHeader = ProtocolHeader.build(uniqueId,deep);
        for (NameValuePair valuePair : protocolHeader.getValuePairs()) {
            build.addHeader(valuePair.getKey(), valuePair.getValue().toString());
        }

        FormBody.Builder formBuilder = new FormBody.Builder();
        for(NameValuePair pair:params){
            try{
                formBuilder.add(pair.getKey(),String.valueOf(pair.getValue()));
            }catch (Exception ignored){}
        }
        build.method("POST",formBuilder.build());
        try {
            logger.info(protocolHeader.toString());
            Response response = client.newCall(build.build()).execute();
            return response.body() != null ? response.body().bytes() : new byte[0];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
