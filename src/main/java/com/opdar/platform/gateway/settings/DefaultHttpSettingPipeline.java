package com.opdar.platform.gateway.settings;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by shiju on 2018/1/4.
 */
public class DefaultHttpSettingPipeline implements SettingPipeline {
    private OkHttpClient client = new OkHttpClient();

    public boolean isUpgrade(){
        return false;
    }

    @Override
    public void upgrade(String url) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            response.body().string();

            String field = "[{\"lengthText\":null,\"path\":\"\\/info\\/get\",\"updateTime\":1514551646000,\"id\":\"a3a77542-809a-4539-8c97-88273c8aedd2\",\"createTime\":1514551646000,\"must\":true,\"checked\":true,\"regex\":false,\"regexErrMsg\":\"\",\"regexText\":\"\",\"lengthErrMsg\":\"\",\"mustErrMsg\":\"用户ID不能为空\",\"routerId\":\"12813e6d-5dde-4bff-b823-84bfe2132acb\",\"checkLength\":false,\"name\":\"userId\",\"type\":\"string\"}]";
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(JsonParser.Feature.ALLOW_MISSING_VALUES,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
