package com.opdar.platform.gateway.settings;

/**
 * Created by shiju on 2018/1/4.
 */
public interface SettingPipeline {
    void upgrade(String url);
}
