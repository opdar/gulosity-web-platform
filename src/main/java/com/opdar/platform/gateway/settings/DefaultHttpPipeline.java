package com.opdar.platform.gateway.settings;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by shiju on 2018/1/4.
 */
public class DefaultHttpPipeline implements Pipeline {
    private OkHttpClient client = new OkHttpClient();

    public boolean isUpgrade(){
        return false;
    }

    @Override
    public void upgrade(String url) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            response.body().string();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(JsonParser.Feature.ALLOW_MISSING_VALUES,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
