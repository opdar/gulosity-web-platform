package com.opdar.platform.gateway.settings;

/**
 * Created by shiju on 2018/1/4.
 */
public interface Pipeline {
    void upgrade(String url);
}
