package com.opdar.platform.gateway.point;

import org.springframework.asm.*;


/**
 * Created by shiju on 2017/12/13.
 */
public class PointClassPrinter extends ClassVisitor {
    private PointManager pointManager;

    public PointClassPrinter(PointManager pointManager) {
        super(Opcodes.ASM4);
        this.pointManager = pointManager;
    }

    private String className = null;

    public void visit(int version, int access, String name, String signature,
                      String superName, String[] interfaces) {
        className = name;
    }

    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        return null;
    }

    public void visitAttribute(Attribute attr) {}

    public void visitEnd() {

    }

    public FieldVisitor visitField(int access, String name, String desc,
                                   String signature, Object value) {
        return null;
    }

    public void visitInnerClass(String name, String outerName,
                                String innerName, int access) {}

    public MethodVisitor visitMethod(int access, String name, String desc,
                                     String signature, String[] exceptions) {
        return new PointMethodPrinter(className,name,pointManager);
    }

    public void visitOuterClass(String owner, String name, String desc) {}

    public void visitSource(String source, String debug) {
    }
}
