package com.opdar.platform.gateway.point;

import com.opdar.platform.utils.MD5;

/**
 * Created by shiju on 2017/12/13.
 */
public class Point {
    private String className;
    private String method;
    private int lineNumber;
    private String code;

    public Point(String className, String method, int lineNumber) {
        this.className = className;
        this.method = method;
        this.lineNumber = lineNumber;
        this.code = MD5.encrypt(className+method+lineNumber);
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode(){
        return code;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
}
