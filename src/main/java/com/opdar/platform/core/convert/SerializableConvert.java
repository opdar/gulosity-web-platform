package com.opdar.platform.core.convert;

public interface SerializableConvert<T> extends ParameterConvert<T> {
    String contentType();
}
