package com.opdar.platform.core.convert;

import java.net.URLDecoder;

/**
 * Created by shiju on 2017/1/18.
 */
public class StringConvert implements ParameterConvert<String> {
    @Override
    public String convert(Object o) throws Exception {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    return URLDecoder.decode(value.toString(),"utf-8");
                }
            }
            return URLDecoder.decode(o.toString(),"utf-8");
        }
        return null;
    }
}
