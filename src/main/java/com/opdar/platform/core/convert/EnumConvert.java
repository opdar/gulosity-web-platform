package com.opdar.platform.core.convert;

/**
 * Created by shiju on 2017/1/18.
 */
public class EnumConvert implements ParameterConvert<Enum> {
    private final Class type;

    public EnumConvert(Class type) {
        this.type = type;
    }

    @Override
    public Enum<?> convert(Object o) {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    return Enum.valueOf(type,value.toString());
                }
            }
            return Enum.valueOf(type,o.toString());
        }
        return null;
    }
}
