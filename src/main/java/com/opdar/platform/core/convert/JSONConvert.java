package com.opdar.platform.core.convert;

import java.lang.reflect.Type;

/**
 * Created by shiju on 2017/12/29.
 */
public abstract class JSONConvert implements SerializableConvert<Object> {
    @Override
    public Object convert(Object o) throws Exception {
        return null;
    }

    public abstract Object convert(Object o,Type type) throws Exception ;
    public abstract byte[] serialization(Object o) throws Exception ;

    @Override
    public String contentType() {
        return "application/json";
    }
}
