package com.opdar.platform.core.convert;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by shiju on 2017/1/19.
 */
public class ListConvertInvocationHandler implements InvocationHandler {
    private ParameterConvert<?> actualTypeConvert;

    public ListConvertInvocationHandler(ParameterConvert<?> actualTypeConvert) {
        this.actualTypeConvert = actualTypeConvert;
    }

    @Override
    public List<Object> invoke(Object proxy, Method method, Object[] args) throws Throwable {
        List<Object> result = new LinkedList<Object>();
        Object o = args[0];
        if(o!=null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                for(Object value:values){
                    value = actualTypeConvert.convert(value);
                    if(value !=null){
                        result.add(value);
                    }
                }
            }else{
                result.add(actualTypeConvert.convert(o));
            }
        }
        return result;
    }
}
