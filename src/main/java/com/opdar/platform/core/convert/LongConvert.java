package com.opdar.platform.core.convert;

/**
 * Created by shiju on 2017/1/18.
 */
public class LongConvert implements ParameterConvert<Long> {
    @Override
    public Long convert(Object o) throws Exception {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    return Long.valueOf(value.toString());
                }
            }
            return Long.valueOf(o.toString());
        }
        return null;
    }
}
