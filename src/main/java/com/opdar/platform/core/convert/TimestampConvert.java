package com.opdar.platform.core.convert;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shiju on 2017/1/18.
 */
public class TimestampConvert implements ParameterConvert<Timestamp> {
    @Override
    public Timestamp convert(Object o) throws Exception {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    if(value != null){
                        return Timestamp.valueOf(value.toString());
                    }
                }
            }
            return Timestamp.valueOf(o.toString());
        }
        return null;
    }
}
