package com.opdar.platform.core.convert;

/**
 * Created by shiju on 2017/1/18.
 */
public interface ParameterConvert<T> {
public T convert(Object o) throws Exception;
}
