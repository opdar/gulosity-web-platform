package com.opdar.platform.core.convert;

/**
 * Created by shiju on 2017/1/18.
 */
public class BoolConvert implements ParameterConvert<Boolean> {
    @Override
    public Boolean convert(Object o) throws Exception {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    return Boolean.valueOf(value.toString());
                }
            }
            return Boolean.valueOf(o.toString());
        }
        return null;
    }
}
