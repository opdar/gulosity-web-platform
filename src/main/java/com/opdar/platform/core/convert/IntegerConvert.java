package com.opdar.platform.core.convert;

/**
 * Created by shiju on 2017/1/18.
 */
public class IntegerConvert implements ParameterConvert<Integer> {
    @Override
    public Integer convert(Object o) throws Exception {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    return Integer.valueOf(value.toString());
                }
            }
            return Integer.valueOf(o.toString());
        }
        return null;
    }
}
