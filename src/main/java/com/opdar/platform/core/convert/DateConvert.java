package com.opdar.platform.core.convert;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shiju on 2017/1/18.
 */
public class DateConvert implements ParameterConvert<Date> {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public Date convert(Object o) throws Exception {
        if(o != null){
            if(o.getClass().isArray()){
                Object[] values = (Object[]) o;
                if(values.length > 0){
                    Object value = values[0];
                    if(value != null){
                        return simpleDateFormat.parse(value.toString());
                    }
                }
            }
            return simpleDateFormat.parse(o.toString());
        }
        return null;
    }
}
