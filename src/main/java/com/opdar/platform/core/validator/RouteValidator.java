package com.opdar.platform.core.validator;

import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.base.Router;
import com.opdar.platform.core.base.RouterManager;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shiju on 2018/1/4.
 */
public class RouteValidator {
    static class FieldValidator {
        private String name;
        private String path;
        private String type;
        private Boolean must;
        private String mustErrMsg;
        private Boolean checkLength;
        private Integer lengthText;
        private String lengthErrMsg;
        private Boolean regex;
        private String regexText;
        private String regexErrMsg;
    }

    private Map<String, Map<String, FieldValidator>> fieldValidatorMap = new HashMap<String, Map<String, FieldValidator>>();

    public boolean validate(String route, int version,String fieldName, Object value) {
        if (RouterManager.contains(route, version)) {
            FieldValidator fieldValidator = fieldValidatorMap.get(route).get(fieldName);
            if (fieldValidator.must && StringUtils.isEmpty(value)) {
                throw new ValidateException(fieldValidator.mustErrMsg);
            }
            if(fieldValidator.checkLength){
                //检查长度
                if(!StringUtils.isEmpty(value)  && String.valueOf(value).length() < fieldValidator.lengthText){
                    throw new ValidateException(fieldValidator.lengthErrMsg);
                }
            }

            if(fieldValidator.regex){
                //检查长度
                if(!StringUtils.isEmpty(value) && !value.toString().matches(fieldValidator.regexText)){
                    throw new ValidateException(fieldValidator.regexErrMsg);
                }
            }
            return true;
        }
        return false;
    }

}
