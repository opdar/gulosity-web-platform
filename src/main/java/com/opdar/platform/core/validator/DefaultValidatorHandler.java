package com.opdar.platform.core.validator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.ReferenceType;
import com.opdar.platform.core.base.ParameterConvertManager;
import com.opdar.platform.core.convert.JSONConvert;
import com.opdar.platform.core.convert.ParameterConvert;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.util.List;

public class DefaultValidatorHandler implements IValidatorHandler {
    private OkHttpClient client = new OkHttpClient();
    private String url;
    public DefaultValidatorHandler(String configsUrl) {
        this.url = configsUrl;
    }

    @Override
    public void getValidator() {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            String fields = response.body().string();
//            String field = "[{\"lengthText\":null,\"path\":\"\\/info\\/get\",\"updateTime\":1514551646000,\"id\":\"a3a77542-809a-4539-8c97-88273c8aedd2\",\"createTime\":1514551646000,\"must\":true,\"checked\":true,\"regex\":false,\"regexErrMsg\":\"\",\"regexText\":\"\",\"lengthErrMsg\":\"\",\"mustErrMsg\":\"用户ID不能为空\",\"routerId\":\"12813e6d-5dde-4bff-b823-84bfe2132acb\",\"checkLength\":false,\"name\":\"userId\",\"type\":\"string\"}]";
            ParameterConvert<?> convert=ParameterConvertManager.getParameterConvert(JSONConvert.class);
            if(convert instanceof JSONConvert){
                ((JSONConvert) convert).convert(fields,new TypeReference<List<RouteValidator.FieldValidator>>(){}.type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
