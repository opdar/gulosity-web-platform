package com.opdar.platform.core.validator;

public class ValidateException extends RuntimeException {
    private final String errMsg;

    public ValidateException(String errMsg) {
        this.errMsg = errMsg;
    }
}
