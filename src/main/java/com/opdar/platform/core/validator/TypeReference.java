package com.opdar.platform.core.validator;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public abstract class TypeReference<T> {
    protected final Type type;

    protected TypeReference() {
        Type superClass = this.getClass().getGenericSuperclass();
        this.type = ((ParameterizedType)superClass).getActualTypeArguments()[0];
    }

    public static void main(String[] args) {
        System.out.println(new TypeReference<List<String>>(){}.type);
    }

    public Type getType() {
        return this.type;
    }
}
