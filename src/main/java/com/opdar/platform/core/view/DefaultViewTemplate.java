package com.opdar.platform.core.view;

import com.opdar.platform.core.base.ViewTemplate;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * JSP RENDER
 * Created by shiju on 2017/1/19.
 */
public class DefaultViewTemplate implements ViewTemplate {
    private String prefix = "/";
    private String suffix = ".jsp";

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public void render(String s, ServletRequest servletRequest, ServletResponse servletResponse) {
        if(servletRequest instanceof HttpServletRequest){
            ServletContext servletContext = ((HttpServletRequest) servletRequest).getSession().getServletContext();
            RequestDispatcher rd = servletContext.getRequestDispatcher(prefix + s + suffix);
            try {
                rd.forward(servletRequest,servletResponse);
            } catch (ServletException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
