package com.opdar.platform.core.interceptors;

/**
 * Created by shiju on 2017/1/19.
 */
public interface Interceptor {
    boolean preRouter();
    boolean postRouter();
}
