package com.opdar.platform.core.base;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.OutputStream;

/**
 * Created by shiju on 2017/1/19.
 */
public interface ViewTemplate {
    void render(String s, ServletRequest servletRequest, ServletResponse servletResponse);
}
