package com.opdar.platform.core.base;

/**
 * Created by shiju on 2017/1/24.
 */
public interface Interceptor {
    boolean before();
    boolean after();
    void finish();
}
