package com.opdar.platform.core.base;

/**
 * Created by shiju on 2017/8/14.
 */
public interface ErrorHandler {
    void handle(Throwable e);
}
