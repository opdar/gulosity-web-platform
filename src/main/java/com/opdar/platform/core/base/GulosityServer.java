package com.opdar.platform.core.base;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 * Created by shiju on 2017/1/23.
 */
public class GulosityServer {

    private int port = 8080;
    private Server server;
    public GulosityServer(int port) {
        this.port = port;
    }

    public void start(){
        server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addEventListener(new ServletEventListener());
        context.setContextPath("/");
        context.addServlet(DispatcherServlet.class, "/");
        server.setHandler(context);
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop(){
        if(server != null){
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                server = null;
            }
        }
    }
}
