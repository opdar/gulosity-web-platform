package com.opdar.platform.core.base;

import com.opdar.platform.annotations.Request;

import java.util.Map;

/**
 * 路由
 * Created by shiju on 2017/1/17.
 */
public class Router {
    private SecretType secretType = SecretType.NONE;
    private String name;
    private String secretKey;
    private int version;
    private Request.Method method = Request.Method.ALL;
    private Request.Format format = Request.Format.JSON;
    private boolean hasToken = false;
    private MethodInvokeHandler handler;

    public Router(String name) {
        this.setName(name);
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public SecretType getSecretType() {
        return secretType;
    }

    public void setSecretType(SecretType secretType) {
        this.secretType = secretType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Request.Method getMethod() {
        return method;
    }

    public void setMethod(Request.Method method) {
        this.method = method;
    }

    public Request.Format getFormat() {
        return format;
    }

    public void setFormat(Request.Format format) {
        this.format = format;
    }

    public boolean isHasToken() {
        return hasToken;
    }

    public void setHasToken(boolean hasToken) {
        this.hasToken = hasToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Router router = (Router) o;

        if (version != router.version) return false;
        if (!name.equalsIgnoreCase(router.name)) return false;
        return router.method == Request.Method.ALL || method == router.method;
    }

    @Override
    public int hashCode() {
        int result = name.toLowerCase().hashCode();
        result = 31 * result + version;
        return result;
    }

    public MethodInvokeHandler getHandler() {
        return handler;
    }

    public void setHandler(MethodInvokeHandler handler) {
        this.handler = handler;
    }

    public boolean interceptorBefore(InterceptorCallback interceptorCallback) {
        boolean ret = true;
        for(Interceptor interceptor:handler.getInterceptors()){
            ret = interceptor.before();
            if(!ret)break;
            else if (interceptor instanceof DefaultInterceptor){
                Map<String, Object> parameters = ((DefaultInterceptor) interceptor).getParameters();
                interceptorCallback.handle(parameters);
            }
        }
        return ret;
    }

    public boolean interceptorAfter() {
        boolean ret = true;
        for(Interceptor interceptor:handler.getInterceptors()){
            ret = interceptor.after();
            if(!ret)break;
        }
        return ret;
    }

    public void interceptorFinish() {
        for(Interceptor interceptor:handler.getInterceptors()){
            interceptor.finish();
        }
    }
}