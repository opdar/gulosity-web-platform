package com.opdar.platform.core.base;

/**
 * Created by shiju on 2017/10/16.
 */
public interface Editor<T,V> {
    public V editor(T result);
}
