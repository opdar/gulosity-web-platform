package com.opdar.platform.core.base;

import java.util.Map;

/**
 * Created by shiju on 2018/2/24.
 */
public interface InterceptorCallback {
    void handle(Map<String, Object> parameters);
}
