package com.opdar.platform.core.session;


/**
 * Created by jeffrey on 2016/11/13.
 */
public interface ISessionManager<T> {
    public void set(String token, T users, long timeout);
    public T get(String token);
    public void remove(String token);
    public void clearTimeout(String token);
    public void destory();
}
