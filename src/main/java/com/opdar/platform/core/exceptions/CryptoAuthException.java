package com.opdar.platform.core.exceptions;

/**
 * Created by shiju on 2017/1/23.
 */
public class CryptoAuthException extends Exception {
    public CryptoAuthException(String message, Throwable cause) {
        super(message, cause);
    }
}
